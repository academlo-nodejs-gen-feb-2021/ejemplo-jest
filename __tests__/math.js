const math = require('../math');
// import * as math from '../math';
// const {suma, resta, crearUsuario, multiplicacion} = require('../math');

describe("Operaciones matematicas", () => {
    test("Probando suma 6 + 5 = 11", () => {
        //AAA
    
        //Arrange
        const num1 = 6;
        const num2 = 5;
        //Act
        const result = math.suma(num1, num2);
        //Assert (Esperamos el resultado sea ...)
        expect(result).toBe(11);
    });

    test("Obtenemos un error cuando la función reciba !== enteros", () => {
        //AAA
    
        //Arrange
        const num1 = "stssring";
        const num2 = 21;
        //Act
        // const result = math.suma(num1, num2);
        //Assert (Esperamos el resultado sea ...)
        expect(() => math.suma(num1, num2)).toThrow("Parametros no aceptados");
    });
    
    test("Probando resta 10 - 3 = 7", () => {
        //AAA
    
        //Arrange
        const num1 = 10;
        const num2 = 3;
        //Act
        const result = math.resta(num1, num2);
        //Assert (Esperamos el resultado sea ...)
        expect(result).toBe(7);
    });
});

describe("Operaciones con objetos", () => {
    test("Construir el objeto usuario", () => {
        //AAA
    
        //Arrange
        const user = {
            name: "Oscar",
            lastname: "Islas",
            email: "oscar.islas@academlo.com"
        };

        //Act
        const result = math.crearUsuario(user.name, user.lastname, user.email);
        //Assert (Esperamos el resultado sea ...)
        expect(result).toEqual(expect.objectContaining({
            name: "Oscar",
            lastname: "Islas",
        })); //Compara de forma estricta todos las propiedad y sus valores
    });
})
