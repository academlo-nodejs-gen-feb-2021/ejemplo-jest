const suma = (a,b) => {
    if(isNaN(Number(a)) || isNaN(Number(b))){
        throw new Error("Parametros no aceptados");
    }
    return a+b;
}

const resta = (a,b) => {
    return a-b;
}

const multiplicacion = (a, b) => {
    return a*b;
}

const crearUsuario = (nombre, apellido, correo) => {
    return {name: nombre, lastname: apellido, email: correo};
}

module.exports = {
    suma,
    resta,
    multiplicacion,
    crearUsuario
}

